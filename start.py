from Proc_18 import Proc18
from Proc_19 import Proc19
from Proc_20 import Proc20


def main():
    t1: Proc18 = Proc18(1, 1)
    t2: Proc19 = Proc19(1, 1, 1)
    t3: Proc20 = Proc20(1, 1)
    tasks: list = [t1, t2, t3]
    for t in tasks:
        t.solve()


if __name__ == '__main__':
    main()
