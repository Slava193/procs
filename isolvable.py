from abc import ABC, abstractmethod


class ISolvable(ABC):
    @abstractmethod
    def solve(self):
        pass