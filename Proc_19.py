import colorama
from isolvable import ISolvable
from singletonmeta import SingletonMeta


class Proc19(ISolvable):
    __metaclass__ = SingletonMeta

    def __init__(self, r1: float,r2: float,p: float):
        self.p = p
        self.r1 = r1
        self.r2 = r2
    def solve(self):
        return self.p*self.r1**2-self.p*self.r2**2
   

# if __name__ == '__main__':
#     r1 = float(input())
#     r2 = float(input())
#     p = 3.14
#     s = Proc_19(r1,r2,p)
#     print(f"{colorama.Fore.BLUE}square: {s.solve()}{colorama.Style.RESET_ALL}")