import colorama
from isolvable import ISolvable
from singletonmeta import SingletonMeta


class Proc20(ISolvable):
    __metaclass__ = SingletonMeta

    def __init__(self, a: float, h: float):

        self.a = a
        self.h = h

    def solve(self):
        return ((((self.a/2)**2+self.h**2)**0.5)*2+self.a)
   

# if __name__ == '__main__':
#     a = float(input())
#     h = float(input())
#     s = Proc_20(a,h)
#     print(f"{colorama.Fore.BLUE}P = {s.solve()}{colorama.Style.RESET_ALL}")