import colorama
from isolvable import ISolvable
from singletonmeta import SingletonMeta


class Proc18(ISolvable):
    __metaclass__ = SingletonMeta

    def __init__(self, r: float, p: float):
        super().__init__()
        self.p = p
        self.r = r

    def solve(self):
        return self.p*self.r**2
   

# if __name__ == '__main__':
#     r = float(input())
#     p = 3.14
#     s = Proc_18(r,p)
#     print(f"{colorama.Fore.BLUE}square: {s.solve()}{colorama.Style.RESET_ALL}")